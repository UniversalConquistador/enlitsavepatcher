﻿namespace EnlitSavePatcher
{
    internal class EnlitSavePatcher
    {
        // Hashing constants
        private const uint HashSeed = 0xC613FC15;
        private const uint HashMultiplier = 0x5BD1E995; // Matches the suggested value in MurmurHash2. Coincidence?
        private const int HashShift = 15;

        // Save file constants hardcoded in the executable
        // These offsets are relative to the null byte that follows the magic ID that starts the file.
        private const int FirstHeaderOffset = 2;
        private const int FirstHeaderLength = 0x10;
        private const int HashOffset = FirstHeaderOffset + FirstHeaderLength;
        private const int JsonOffset = 0x22;

        public static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: EnlitSavePatcher <filename>\n\n   or drag and drop your catc.save file on this exe.");
                return;
            }

            string filename = args[0];

            byte[] file;
            try
            {
                file = System.IO.File.ReadAllBytes(filename);
            }
            catch (IOException ex)
            {
                Console.WriteLine("Failed to read file. Details:\n\n");
                Console.WriteLine(ex.ToString());
                return;
            }

            // Measure how long the magic ID is (i.e. look for the first null byte)
            int magicIDLength = -1;
            for (int i = 0; i < file.Length; i++)
            {
                if (file[i] == 0)
                {
                    magicIDLength = i;
                    break;
                }
            }

            if (magicIDLength == -1)
            {
                Console.WriteLine("Invalid save file - does not start with expected magic ID!");
                return;
            }

            ReadOnlySpan<byte> firstHeaderData = file.AsSpan(magicIDLength + EnlitSavePatcher.FirstHeaderOffset, EnlitSavePatcher.FirstHeaderLength);
            uint existingHash = EnlitSavePatcher.ReverseEndian(BitConverter.ToUInt32(file.AsSpan(magicIDLength + EnlitSavePatcher.HashOffset, 4)));

            ReadOnlySpan<byte> jsonData = file.AsSpan(magicIDLength + EnlitSavePatcher.JsonOffset);

            // Calculate the hash of the json and header data
            uint hash = EnlitSavePatcher.HashSeed;
            EnlitSavePatcher.UpdateHash(jsonData, ref hash);
            EnlitSavePatcher.UpdateHash(firstHeaderData, ref hash);

            if (hash != existingHash)
            {
                Console.WriteLine("Outdated hash - updating file.");
                BitConverter.TryWriteBytes(file.AsSpan(magicIDLength + EnlitSavePatcher.HashOffset, 4), EnlitSavePatcher.ReverseEndian(hash));

                try
                {
                    System.IO.File.WriteAllBytes(filename, file);
                    Console.WriteLine("Done.");
                }
                catch (IOException ex)
                {
                    Console.WriteLine("Failed to write file. Details:\n\n");
                    Console.WriteLine(ex.ToString());
                }
            }
            else
            {
                Console.WriteLine("Hash verified.");
            }
        }

        private static uint ReverseEndian(uint value)
        {
            return (value & 0x000000FFU) << 24 | (value & 0x0000FF00U) << 8 | (value & 0x00FF0000U) >> 8 | (value & 0xFF000000U) >> 24;
        }

        // This doesn't really look like MurmurHash2 to me despite the choice of constant, but I'm no cryptographer
        private static void UpdateHash(ReadOnlySpan<byte> data, ref uint hash)
        {
            // The enlit implementation has an unfortunate snag of reading the bytes as chars, which are signed
            // when compiling for Windows, which then get sign-extended when negative (movsx).
            // I got hung up on this snag for longer than I should have.

            int length = data.Length;
            int unalignedLength = length & 3;
            int alignedLength = length - unalignedLength;

            for (int i = 0; i < alignedLength; i += 4)
            {
                uint v1 = EnlitSavePatcher.HashMultiplier * (hash ^ (uint)(sbyte)data[i]);
                uint v2 = EnlitSavePatcher.HashMultiplier * ((v1 >> HashShift) ^ v1 ^ (uint)(sbyte)data[i + 1]);
                uint v3 = EnlitSavePatcher.HashMultiplier * ((v2 >> HashShift) ^ v2 ^ (uint)(sbyte)data[i + 2]);
                hash = (EnlitSavePatcher.HashMultiplier * ((v3 >> HashShift) ^ v3 ^ (uint)(sbyte)data[i + 3]))
                    ^ ((EnlitSavePatcher.HashMultiplier * ((v3 >> HashShift) ^ v3 ^ (uint)(sbyte)data[i + 3])) >> HashShift);
            }

            for (int i = alignedLength; i < length; i++)
            {
                hash = (EnlitSavePatcher.HashMultiplier * (hash ^ (uint)(sbyte)data[i]))
                    ^ ((EnlitSavePatcher.HashMultiplier * (hash ^ (uint)(sbyte)data[i])) >> HashShift);
            }
        }
    }
}