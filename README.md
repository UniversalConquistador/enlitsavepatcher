﻿Enlit Save Patcher
==================

This program updates the hash in the savegame files for Enlit3d games (currently tested against Corrupted at the Core).

Usage:
 - Make your modifications to your save file (e.g. `catc.sav`)
 - Drag the save file onto `EnlitSavePatcher.exe`

If you run it from the command line, like so:
```
EnlitSavePatcher.exe "catc.sav"
```
Then you can see the output indicating whether the savegame was patched, or whether an error occurred.